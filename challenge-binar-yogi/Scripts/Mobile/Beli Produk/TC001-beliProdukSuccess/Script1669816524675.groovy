import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/jc-0011xbinar/Documents/challenge binar/automation/challenge-binar-yogi/apk/second-hand.apk', 
    false)

Mobile.tap(findTestObject('Mobile/login/men-akun'), 0)

Mobile.tap(findTestObject('Mobile/login/button-masuk'), 0)

Mobile.setText(findTestObject('Mobile/login/enter-email'), 'yogisetiawan931@gmail.com', 0)

Mobile.setText(findTestObject('Mobile/login/enter-password'), 'tes1234567', 0)

Mobile.tap(findTestObject('Mobile/login/button-masuk (1)'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Beli Produk/menu_beranda'), 0)

Mobile.delay(30, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Beli Produk/card_produk'), 0)

Mobile.tap(findTestObject('Mobile/Beli Produk/btn_tertarikNego'), 0)

Mobile.setText(findTestObject('Mobile/Beli Produk/enter_hargaTawar'), '1000000', 0)

Mobile.tap(findTestObject('Mobile/Beli Produk/btn_kirim'), 0)

Mobile.closeApplication()

